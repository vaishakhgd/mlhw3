#from classification import *
import pandas as pd
#as per tutorial https://www.youtube.com/watch?v=8aZNAmWKGfs
df= pd.read_csv("filtered_data_alpha.csv")
df_wordcount = pd.read_csv("wordcount.csv")
df_wordcount_without_type= pd.read_csv("wordcount_without_type_for_laplace.csv")
class SpamCalculator2:
    words_arr= []
    def __init__(self,words_arr):
        pass
        self.words_arr= words_arr
    def p_spam(self):
        global df
        return df[df["type"]=="spam"].shape[0]/(df[df["type"]=="ham"].shape[0]+df[df["type"]=="spam"].shape[0])
    def p_ham(self):
        global df
        return df[df["type"] == "ham"].shape[0] / (
                    df[df["type"] == "ham"].shape[0] + df[df["type"] == "spam"].shape[0])
    def getSpamProbability(self,dat_word):
        alpha=0.01
        V = df_wordcount_without_type.shape[0]  # dimension

        df2 = df_wordcount[df_wordcount["text"] == dat_word]

        df3 = df2[df2["type"] == 'spam']


        df_spam = df[df['type'] == 'spam']
        numerator = alpha
        denominator = df_spam.shape[0] + alpha * (V + 1)
        for index, row in df3.iterrows():

            numerator += row['count']
            break
        return numerator / denominator


    def getHamProbability(self,dat_word):
        alpha=0.01
        V = df_wordcount_without_type.shape[0]  # dimension

        df2 = df_wordcount[df_wordcount["text"] == dat_word]

        df3 = df2[df2["type"] == 'ham']


        df_ham = df[df['type'] == 'ham']
        numerator = alpha
        denominator = df_ham.shape[0] + alpha * (V + 1)
        for index, row in df3.iterrows():

            numerator += row['count']
            break
        return numerator / denominator



    def prob_sentence_given_spam_ham(self):
        p_sentence_given_spam=1
        p_sentence_given_ham = 1
        for index,row in df_wordcount_without_type.iterrows():
            dat_word=str(row['text'])
            spam_prob=self.getSpamProbability(dat_word)
            ham_prob = self.getHamProbability(dat_word)
            if(dat_word in self.words_arr):
                p_sentence_given_spam*=spam_prob
                p_sentence_given_ham*=ham_prob
            else:
                p_sentence_given_spam *=(1-spam_prob)
                p_sentence_given_ham *= (1 - ham_prob)

        denominator = (p_sentence_given_spam*self.p_spam()+p_sentence_given_ham*self.p_ham())
        probability_of_spam_given_words = p_sentence_given_spam*self.p_spam()/denominator
        print("probability_of_spam_given_words ",probability_of_spam_given_words)
        probability_of_ham_given_words = p_sentence_given_ham*self.p_ham()/denominator
        print("probability_of_ham_given_words ", probability_of_ham_given_words)
        if(probability_of_spam_given_words>probability_of_ham_given_words):
            print("its SPAM")
            return 'spam'
        else:
            print("ITS HAM ")
            return 'ham'


spamcalc = SpamCalculator2(['wonderful','hi','with','me','sex'])
#spamcalc = SpamCalculator(['hi','there'])
spamcalc.prob_sentence_given_spam_ham()













