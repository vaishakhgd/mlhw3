print("HI")
f=open("SMSSpamCollection","r")
import pandas as pd
dataset= {'type':[],'text':[]}

from Nltk import *

def createDataset():
    for i in f.readlines():
        dat=i.split()
        dataset['type'].append(dat[0])
        s= " ".join(dat[1:])
        s=s.strip()
        s=s.lower()
        nltkObj = Nltk(s)
        s= nltkObj.removeStopwords()
        dataset['text'].append(s)

createDataset()
df = pd.DataFrame(dataset,columns= ['type', 'text'])
print(df)

df.to_csv("filtered_data_alpha.csv",sep=",",header=True)




