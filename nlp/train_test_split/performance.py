import pandas as pd
import nltk
from nltk import word_tokenize
from classification import *
from PerformanceMeasure import *

#df = df.sample(frac=0.50)
df = pd.read_csv("filtered_data_alpha.csv")
df_test = pd.read_csv("test.csv")
class Performance():
    correct_classification_no=0
    def __init__(self):
        pass
    def return_training_set(self):
        df_spam= df[df['type']=='spam']
        df_ham = df[df['type'] == 'ham']
        df_spam_sample = df_spam.sample(frac=0.2)
        print(df_spam_sample.shape)
        df_ham_sample = df_ham.sample(frac=0.2)
        print(df_ham_sample.shape)
        df_sample = df_spam_sample.append(df_ham_sample)
        print(df_sample.shape)
        global df_test
        #return df_sample
        return df_test
    def create_word_arr(self):
        self.correct_classification_no=0
        df_sample= self.return_training_set()
        performance = PerformanceMeasure(fn=0,fp=0,tp=0,tn=0)
        for index,row in df_sample.iterrows():
            sentence = str(row['text'])
            word_arr = nltk.word_tokenize(sentence)
            spamcalc = SpamCalculator(word_arr)
            decision = spamcalc.makeDcision()
            if(decision==str(row['type'])):
                self.correct_classification_no+=1
            if (decision == str(row['type']) and  decision=='ham'):
                performance.tp+=1
            if (decision == str(row['type']) and decision == 'spam'):
                performance.tn += 1
            if (decision == 'ham' and str(row['type'])=='spam'):
                performance.fp += 1
            if (decision == 'spam' and str(row['type']) == 'ham'):
                performance.fn += 1





        print("self.correct_classification_no ",self.correct_classification_no)
        print("df_test shape is ",df_sample.shape)
        print("Accuracy is ",self.correct_classification_no/df_sample.shape[0])
        performance.precision()
        performance.f1_score()
        performance.false_pos()
        performance.specifity()
        performance.true_pos_recall_sensitivity()





p = Performance()
p.create_word_arr()

