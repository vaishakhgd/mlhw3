print("HI")
f=open("SMSSpamCollection","r")
import pandas as pd
dataset= {'type':[],'text':[]}

from Nltk import *

def createDataset():
    for i in f.readlines():
        dat=i.split()
        dataset['type'].append(dat[0])
        s= " ".join(dat[1:])
        s=s.strip()
        s=s.lower()
        nltkObj = Nltk(s)
        s= nltkObj.removeStopwords()
        dataset['text'].append(s)

createDataset()
df = pd.DataFrame(dataset,columns= ['type', 'text'])
print(df)
from sklearn.model_selection import train_test_split
train, test = train_test_split(df, test_size=0.3)

train.to_csv("filtered_data_alpha.csv",sep=",",header=True)

test.to_csv("test.csv",sep=",",header=True)






