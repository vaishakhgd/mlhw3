import pandas as pd

import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from mpl_toolkits.mplot3d import Axes3D
from mlxtend.plotting import plot_decision_regions
from copy import deepcopy

dataset = {'height': [170,190,150,168,175,185,171,155,165],
              'weight': [57,95,45,65,78,90,65,48,60],
              'age': [32,28,35,29,26,32,28,31,27],
              'Y': [1,0,1,0,0,0,1,1,1]
              }




self_train_dataset = {'height': [],
              'weight': [],
              'age': [],
              'Y': []
              }

df = pd.DataFrame(dataset,columns= ['height', 'weight','age','Y'])


#print (df)

#X = df[['height', 'weight','age']]

Du=[(182,80,30),(175,69,28),(178,80,27),(160,50,31),(170,72,30),
    (152,45,29),(177,79,28),(171,62,27),(185,90,30),(181,83,28),
    (168,59,24),(158,45,28),(178,82,28),(165,55,30),(162,58,28),
    (180,80,29),(173,75,28),(172,65,27),(160,51,29),(178,77,28),(182,84,27),
    (175,67,28),(163,50,27),(177,80,30),(170,65,28)]

Du=[list(x) for x in Du]

def predict():
    pass
    X = df[['height', 'weight', 'age']]
    y = df['Y']
    dataset_test = {'height': [180, 169],
                    'weight': [80, 75],
                    'age': [30, 27]

                    }

    df_test = pd.DataFrame(dataset_test, columns=['height', 'weight', 'age'])

    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)

    y_pred = logistic_regression.predict(df_test)
    print(y_pred)
    #print("predict_proba ",logistic_regression.predict_proba(df_test))

W=1
M=0
D =[(170,57,32,  W),
 (190,95,28,  M),
 (150,45,35,  W),(168,65,29,  M),
 (175,78,26,  M),(185,90,32,  M),
 (171,65,28,  W),(155,48,31,  W),(165,60,27,  W)]



D = [list(x) for x in D]

D2 = deepcopy(D)

def getdistance(pt1,pt2):
    return ((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2+(pt1[2]-pt2[2])**2)**0.5




predict()

def saveplot(iteration_no):
    global self_train_dataset
    df = pd.DataFrame(self_train_dataset, columns=['height', 'weight', 'age', 'Y'])
    # --------------------------------------------------------------------------
    X = df[['age', 'weight']]
    y = df['Y']
    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression)
    plt.savefig('age_weight'+str(iteration_no)+".png")
    plt.show()
    #--------------------------------------------------------------------------
    X = df[['height', 'weight']]
    y = df['Y']
    logistic_regression2 = LogisticRegression()
    logistic_regression2.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression2)
    plt.savefig('height_weight' + str(iteration_no) + ".png")
    plt.show()
    # --------------------------------------------------------------------------
    X = df[['height', 'age']]
    y = df['Y']
    logistic_regression3 = LogisticRegression()
    logistic_regression3.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression3)

    plt.savefig('height_age' + str(iteration_no) + ".png")
    plt.show()


def getTwoNearest(iteration_no):
    max_dist=-1
    i1=0
    j1=0
    for i in range(len(D2)):
        for j in range(len(D2)):
            if(i!=j and D2[i][3]!=D2[j][3]):#two farther elements must belong to different classes
                d= getdistance(D2[i],D2[j])
                if(d>max_dist):
                    max_dist=d
                    i1=i
                    j1=j
    print("two farthest elements are ",D2[i1]," ",D2[j1]," ",max_dist)
    print("i1,j1,D2",i1,",",j1,",",D2)
    if (i1 != j1):
        self_train_dataset['height'].append(D2[i1][0])
        self_train_dataset['height'].append(D2[j1][0])
        self_train_dataset['weight'].append(D2[i1][1])
        self_train_dataset['weight'].append(D2[j1][1])
        self_train_dataset['age'].append(D2[i1][2])
        self_train_dataset['age'].append(D2[j1][2])
        self_train_dataset['Y'].append(D2[i1][3])
        self_train_dataset['Y'].append(D2[j1][3])
        print(self_train_dataset)
    else:
        self_train_dataset['height'].append(D2[i1][0])
        self_train_dataset['weight'].append(D2[i1][1])
        self_train_dataset['age'].append(D2[i1][2])
        self_train_dataset['Y'].append(D2[i1][3])
        print(self_train_dataset)

    saveplot(iteration_no)
    if(len(D2)>0 and i1<len(D2)):
        del D2[i1]
    if (len(D2) > 0 and j1<len(D2)):
        del D2[j1]
    #print("D2 after removal is ",D2)


def addFinalGraph():
    df = pd.DataFrame(dataset, columns=['height', 'weight', 'age', 'Y'])
    # --------------------------------------------------------------------------
    X = df[['age', 'weight']]
    y = df['Y']
    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression)
    plt.savefig('age_weight_final' + ".png")
    plt.show()
    #------------
    X = df[['height', 'weight']]
    y = df['Y']
    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression)
    plt.savefig('height_weight_final' + ".png")
    plt.show()
    #--------------
    X = df[['age', 'height']]
    y = df['Y']
    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression)
    plt.savefig('age_height_final' + ".png")
    plt.show()


count=0
while(len(D2)>0 ):
    count+=1
    getTwoNearest(count)


addFinalGraph()

dictionary_of_priorities={}


def plotProbability():
    K=3
    global dataset
    global dictionary_of_priorities
    dataset2=deepcopy(dataset)
    dictionary_of_priorities2 = deepcopy(dictionary_of_priorities)
    key1 = max(dictionary_of_priorities2.keys())
    val1 = dictionary_of_priorities2[key1]
    del dictionary_of_priorities2[key1]
    key2 = max(dictionary_of_priorities2.keys())
    val2 = dictionary_of_priorities2[key2]
    del dictionary_of_priorities2[key2]
    key3 = max(dictionary_of_priorities2.keys())
    val3 = dictionary_of_priorities2[key3]

    dataset2['height'].append(val1[0])
    dataset2['height'].append(val2[0])
    dataset2['height'].append(val3[0])

    dataset2['weight'].append(val1[1])
    dataset2['weight'].append(val2[1])
    dataset2['weight'].append(val3[1])

    dataset2['age'].append(val1[2])
    dataset2['age'].append(val2[2])
    dataset2['age'].append(val3[2])

    dataset2['Y'].append(val1[3])
    dataset2['Y'].append(val2[3])
    dataset2['Y'].append(val3[3])

    df = pd.DataFrame(dataset2, columns=['height', 'weight', 'age', 'Y'])

    X = df[['age', 'weight']]
    y = df['Y']
    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression)
    plt.savefig('K=3_age_weight'  + ".png")
    plt.show()
    # --------------------------------------------------------------------------
    X = df[['height', 'weight']]
    y = df['Y']
    logistic_regression2 = LogisticRegression()
    logistic_regression2.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression2)
    plt.savefig('K=3_height_weight'  + ".png")
    plt.show()
    # --------------------------------------------------------------------------
    X = df[['height', 'age']]
    y = df['Y']
    logistic_regression3 = LogisticRegression()
    logistic_regression3.fit(X, y)
    X_numpy = X.to_numpy()
    y_numpy = y.to_numpy()
    plot_decision_regions(X_numpy, y_numpy, clf=logistic_regression3)

    plt.savefig('K=3_height_age'  + ".png")
    plt.show()








def addPredProbability():
    pass
    X = df[['height', 'weight', 'age']]
    y = df['Y']
    dataset_test = {'height': [],
                    'weight': [],
                    'age': []

                    }



    logistic_regression = LogisticRegression()
    logistic_regression.fit(X, y)

    #y_pred = logistic_regression.predict(df_test)
    #print(y_pred)
    #prob = logistic_regression.predict_proba(df_test)
    #print(prob[0][0])
    #print(prob[0][1])
    #print(prob[1][0])
    for i in range(len(Du)):
        dataset_test['height'].append(Du[i][0])
        dataset_test['weight'].append(Du[i][1])
        dataset_test['age'].append(Du[i][2])
    df_test = pd.DataFrame(dataset_test, columns=['height', 'weight', 'age'])
    y_pred = logistic_regression.predict(df_test)
    prob = logistic_regression.predict_proba(df_test)
    for i in range(len(y_pred)):
        d2 = deepcopy(Du)
        d2[i].append(y_pred[i])
        prpb_val = max(prob[i][0],prob[i][1])
        dictionary_of_priorities[prpb_val] = d2[i]
    print("------------------------------------------------------------------------------------")
    print(dictionary_of_priorities)
    print("------------------------------------------------------------------------------------")
    plotProbability()

addPredProbability()











