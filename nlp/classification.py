test_data=[['hi'],['there']]
#as per https://medium.com/syncedreview/applying-multinomial-naive-bayes-to-nlp-problems-a-practical-explanation-4f5271768ebf
import pandas as pd

df= pd.read_csv("filtered_data_alpha.csv")
df_wordcount = pd.read_csv("wordcount.csv")
df_wordcount_without_type= pd.read_csv("wordcount_without_type_for_laplace.csv")
#print(df[df["type"]=="ham"].shape[0])
class Classifier:
    token=''

    def __init__(self,token):
        pass
        self.token=token
    def p_spam(self):
        global df
        return df[df["type"]=="spam"].shape[0]/(df[df["type"]=="ham"].shape[0]+df[df["type"]=="spam"].shape[0])
    def p_ham(self):
        global df
        return df[df["type"] == "ham"].shape[0] / (
                    df[df["type"] == "ham"].shape[0] + df[df["type"] == "spam"].shape[0])
    def p_word_given_spam(self):
        alpha=0.01
        V=df_wordcount_without_type.shape[0] #dimension

        df2 = df_wordcount[df_wordcount["text"]==self.token]
        #print(df2)
        df3=  df2[df2["type"] == 'spam']
        #print(df3)
        #print(df3.shape)
        df_spam = df[df['type']=='spam']
        numerator = alpha
        denominator = df_spam.shape[0] + alpha*(V+1)
        for index,row in df3.iterrows():
            #laplace_prob = (row['count']+alpha)
            numerator+=row['count']
            break
        #print("------------------------------")
        #print(type(numerator/denominator))
        #print(numerator/denominator)
        #print("------------------------------")
        return numerator / denominator

    def p_word_given_ham(self):
        alpha=0.01
        V=df_wordcount_without_type.shape[0] #dimension

        df2 = df_wordcount[df_wordcount["text"]==self.token]
        #print(df2)
        df3=  df2[df2["type"] == 'ham']
        #print(df3)
        #print(df3.shape)
        df_ham = df[df['type']=='ham']
        numerator = alpha
        denominator = df_ham.shape[0] + alpha*(V+1)
        for index,row in df3.iterrows():
            #laplace_prob = (row['count']+alpha)
            numerator+=row['count']
            break
        #print("------------------------------")
        #print(type(numerator/denominator))
        #print(numerator/denominator)
        #print("------------------------------")
        return numerator/denominator




class SpamCalculator:
    words_arr=[]
    def __init__(self,words_arr):
        self.words_arr=words_arr
    def product_of_probability_word_given_spam(self):
        product=1
        for word in self.words_arr:
            classi = Classifier(word)
            product*=classi.p_word_given_spam()
        return product



    def product_of_probability_word_given_ham(self):
        product = 1
        for word in self.words_arr:
            classi = Classifier(word)
            product *= classi.p_word_given_ham()
        return product
    def probability_of_spam_given_words(self):
        classi = Classifier('')
        num = self.product_of_probability_word_given_spam()*classi.p_spam()
        denom = self.product_of_probability_word_given_spam()*classi.p_spam() + self.product_of_probability_word_given_ham()*classi.p_ham()
        print("probability_of_spam_given_words ",num/denom)
        return num/denom


    def probability_of_ham_given_words(self):
        classi = Classifier('')
        num = self.product_of_probability_word_given_ham()* classi.p_ham()
        denom = self.product_of_probability_word_given_ham() * classi.p_ham() + self.product_of_probability_word_given_spam() * classi.p_spam()
        print("probability_of_ham_given_words ", num / denom)
        return num / denom

    def makeDcision(self):
        spam_prob = self.probability_of_spam_given_words()
        ham_prob = self.probability_of_ham_given_words()
        if(spam_prob>ham_prob):
            print("Its SPAM")
            return 'spam'
        else:
            print("ITS HAM")
            return 'ham'





#classi=Classifier('mono')


spamcalc = SpamCalculator(['wonderful','hi','with','me','sex','call'])
print(spamcalc.makeDcision())










'''
import nltk
from nltk import word_tokenize
from nltk.util import ngrams
text = "Hi How are you i am fine and you"
token=nltk.word_tokenize(text)
bigrams=ngrams(token,2)
print(list(bigrams))
'''