import pandas as pd
df = pd.read_csv("filtered_data_alpha.csv")
dataset = {'type':[],'text':[],'count':[]}

for index, row in df.iterrows():
    row1_text= str(row['text'])
    row1_text.strip()
    type = row['type']
    print(row1_text)
    arr = row1_text.split()
    dictionary_for_ensuring_repeated_words_are_consideredonce={}
    for i in arr:
        dictionary_for_ensuring_repeated_words_are_consideredonce[i]=type
    for wordKey in dictionary_for_ensuring_repeated_words_are_consideredonce.keys():
        dataset['text'].append(wordKey)
        dataset['count'].append(1)
        dataset['type'].append(dictionary_for_ensuring_repeated_words_are_consideredonce[wordKey])

df = pd.DataFrame(dataset,columns= ['type', 'text','count'])

df.groupby(['type', 'text']).agg({'count':'sum'}).to_csv("wordcount.csv",sep=",",header=True)

df.groupby(['text']).agg({'count':'sum'}).to_csv("wordcount_without_type_for_laplace.csv",sep=",",header=True)





