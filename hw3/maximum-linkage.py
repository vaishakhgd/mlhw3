import math

D={(170,57,32),(190,95,28),(150,45,35),(168,65,29),(175,78,26),(185,90,32),(171,65,28),(155,48,31),(165,60,27),(182,80,30),(175,69,28),(178,80,27),(160,50,31),(170,72,30)}

W='W'
M='M'
D={(170,57,32,W),(190,95,28,M),(150,45,35,W),(168,65,29,M),(175,78,26,M),(185,90,32,M),(171,65,28,W),(155,48,31,W),(165,60,27,W),(182,80,30,M),(175,69,28,W),(178,80,27,M),(160,50,31,W),(170,72,30,M)}


D=[list(x) for x in D]

import logging
logging.basicConfig(filename='maximum-linkage.log', encoding='utf-8', level=logging.DEBUG)


clusters = {}

clusters_with_gender_index={}

for i in range(len(D)):
    clusters[i]=[D[i]]
    if(D[i][3]==W):
        clusters_with_gender_index[i]={W:1,M:0}
    else:
        clusters_with_gender_index[i] = {M: 1, W: 0}



print(clusters)
def getdistance(pt1,pt2):
    pass
    return ((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2+(pt1[2]-pt2[2])**2)**0.5


def getInterClusterDistance(cluster,joining_cluster):
    cluster_pts = clusters[cluster]
    joining_cluster_pts =clusters[joining_cluster]
    max_cluster_distance=0
    for pt1 in cluster_pts:
        for pt2 in joining_cluster_pts:
            distance = getdistance(pt1,pt2)
            if(distance>max_cluster_distance):
                max_cluster_distance=distance
    return max_cluster_distance





def modifyGenderIndex():
    pass
    misclassified_pts=0
    global clusters_with_gender_index
    clusters_with_gender_index={}
    for cluster in clusters.keys():
        clusters_with_gender_index[cluster] = {W: 0, M: 0}
        for pt in clusters[cluster]:
            if(pt[3]==W):
                clusters_with_gender_index[cluster][W]+=1
            if (pt[3] == M):
                clusters_with_gender_index[cluster][M] += 1

    for cluster in clusters_with_gender_index.keys():
        rate = min(clusters_with_gender_index[cluster][W],clusters_with_gender_index[cluster][M])
        misclassified_pts+=rate
    #super_logger = setup_logger('second_logger', 'maximum-misclassification.log')
    f=open("maximum-misclassification-rate.txt","a+")
    f.write(str(clusters_with_gender_index)+"--------misclassified pts are ----- "+ str(misclassified_pts))
    f.write("\n")
    #super_logger.info(str(clusters_with_gender_index)+"--------misclassified pts are ----- "+ str(misclassified_pts))












def getMaximumDistance():
    max_joinng_inter_cluster_distance=0
    joning_clusters=[0,0]
    for cluster in clusters.keys():
        for joining_cluster in clusters.keys():
            if(cluster!=joining_cluster):
                dist = getInterClusterDistance(cluster,joining_cluster)
                if(dist>max_joinng_inter_cluster_distance):
                    max_joinng_inter_cluster_distance=dist
                    joning_clusters=[cluster,joining_cluster]
    print("joining clusters are ",joning_clusters,"with distance as ",max_joinng_inter_cluster_distance)
    clusters[joning_clusters[0]] += clusters[joning_clusters[1]]
    del clusters[joning_clusters[1]]
    modifyGenderIndex()
    #basic_logger = setup_logger('basic_logger', 'maximum-linkage.log')
    logging.info(str(clusters))



while(len(clusters)>1):
    getMaximumDistance()










