import pandas as pd

import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from mpl_toolkits.mplot3d import Axes3D
from mlxtend.plotting import plot_decision_regions

#Ds={((170,57,32),  W),
# ((190,95,28),  M),
# ((150,45,35),  W),((168,65,29),  M),
# ((175,78,26),  M),((185,90,32),  M),
# ((171,65,28),  W),((155,48,31),  W),((165,60,27),  W)}


#Du={(182,80,30),(175,69,28),(178,80,27),(160,50,31),
# (170,72,30),(152,45,29),(177,79,28),(171,62,27),(185,90,30),
# (181,83,28),(168,59,24),(158,45,28),(178,82,28),(165,55,30),
# (162,58,28),
# (180,80,29),(173,75,28),(172,65,27),(160,51,29),(178,77,28),
# (182,84,27),(175,67,28),(163,50,27),(177,80,30),(170,65,28)}

dataset = {'height': [170,190,150,168,175,185,171,155,165],
              'weight': [57,95,45,65,78,90,65,48,60],
              'age': [32,28,35,29,26,32,28,31,27],
              'Y': [1,0,1,0,0,0,1,1,1]
              }

dataset2 = {'height': [170,190],
              'weight': [57,95],
              'age': [32,28],
              'Y': [1,0]
              }

df = pd.DataFrame(dataset,columns= ['height', 'weight','age','Y'])
#print (df)

#X = df[['height', 'weight','age']]
#X = df[['height', 'weight']]
X = df[['age', 'weight','height']]
y = df['Y']

dataset_test = {'height': [180,169],
              'weight': [80,75],
              'age': [30,27]

              }

df_test = pd.DataFrame(dataset_test,columns= ['height', 'weight','age'])


logistic_regression= LogisticRegression()
logistic_regression.fit(X,y)




y_pred=logistic_regression.predict(df_test)

print(y_pred)

print("predict_proba ",logistic_regression.predict_proba(df_test))


'''
X_height =df['height']

X_numpy= X.to_numpy()

y_numpy= y.to_numpy()
plot_decision_regions(X_numpy, y_numpy,clf=logistic_regression)
plt.savefig('1.png')

#plt.show()

#X_numpy= df[[ 'weight','age']].to_numpy()
plot_decision_regions(X_numpy, y_numpy,clf=logistic_regression)
plt.show()

#plt.scatter(X,y)
'''
