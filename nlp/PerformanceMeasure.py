
class PerformanceMeasure:
    tp=0
    fp=0
    tn=0
    fn=0
    def __init__(self,tp,tn,fp,fn):
        self.tp = tp
        self.tn = tn
        self.fp=fp
        self.fn = fn
    def precision(self):
        precision = self.tp/(self.tp+self.fp)
        print("precision is ",precision)
    def false_pos(self):
        false_positive = self.fp / (self.tn + self.fp)
        print("false_positive is ", false_positive)
    def specifity(self):
        specifity = self.tn / (self.tn + self.fp)
        print("specifity is ", specifity)

    def true_pos_recall_sensitivity(self):
        true_pos = self.tp / (self.tp + self.fn)
        print("true_pos_recall_sensitivity is ", true_pos)
    def f1_score(self):
        precision = self.tp / (self.tp + self.fp)
        recall = self.tp / (self.tp + self.fn)
        print("f1 score ",2*(precision*recall)/(precision+recall))






