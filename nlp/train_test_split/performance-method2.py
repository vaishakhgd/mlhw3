import pandas as pd
import nltk
from nltk import word_tokenize
from classification import *
from classificationmethod2 import *
#df = df.sample(frac=0.50)
df = pd.read_csv("filtered_data_alpha.csv")
df_test = pd.read_csv("test.csv")
class Performance():
    correct_classification_no=0
    def __init__(self):
        pass
    def return_training_set(self):
        df_spam= df[df['type']=='spam']
        df_ham = df[df['type'] == 'ham']
        df_spam_sample = df_spam.sample(frac=0.2)
        print(df_spam_sample.shape)
        df_ham_sample = df_ham.sample(frac=0.2)
        print(df_ham_sample.shape)
        df_sample = df_spam_sample.append(df_ham_sample)
        print(df_sample.shape)
        global df_test
        #return df_sample
        return df_test
    def create_word_arr(self):
        self.correct_classification_no=0
        df_sample= self.return_training_set()

        for index,row in df_sample.iterrows():
            sentence = str(row['text'])
            word_arr = nltk.word_tokenize(sentence)
            spamcalc = SpamCalculator2(word_arr)
            decision = spamcalc.prob_sentence_given_spam_ham()
            if(decision==str(row['type'])):
                self.correct_classification_no+=1
        print("self.correct_classification_no ",self.correct_classification_no)
        print("df_test shape is ",df_sample.shape)
        print("Accuracy is ",self.correct_classification_no/df_sample.shape[0])





p = Performance()
p.create_word_arr()
