import pandas as pd
import nltk
from nltk import word_tokenize
from nltk.util import ngrams
from copy import deepcopy

df = pd.read_csv("filtered_data_alpha.csv")
dataset = {'type':[],'text':[],'count':[]}


class Ngrams:
    words_arr=[]
    def __init__(self,words_arr):
        self.words_arr= words_arr
    def makeStringWithNgrams(self,ngramlist):
        pass
        return "^".join(ngramlist)
    def return_arr_with_n_gram_words(self):
        words_arr_copy = deepcopy(self.words_arr)
        bigrams = ngrams(words_arr_copy, 2)
        bigrams = [list(x) for x in bigrams]
        bigrams = [self.makeStringWithNgrams(x) for x in bigrams]
        print("bigrams",bigrams)
        trigrams = ngrams(words_arr_copy, 3)
        trigrams = [list(x) for x in trigrams]
        trigrams = [self.makeStringWithNgrams(x) for x in trigrams]
        print("trigrams", trigrams)
        fourgrams = ngrams(words_arr_copy, 4)
        fourgrams = [list(x) for x in fourgrams]
        fourgrams = [self.makeStringWithNgrams(x) for x in fourgrams]
        print("fourgrams", fourgrams)
        if(len(bigrams)>0):
            words_arr_copy+=bigrams

        return words_arr_copy

for index, row in df.iterrows():
    row1_text= str(row['text'])
    row1_text.strip()
    type = row['type']
    print(row1_text)
    arr = row1_text.split()
    ngrams_obj = Ngrams(arr)
    arr = ngrams_obj.return_arr_with_n_gram_words()
    dictionary_for_ensuring_repeated_words_are_consideredonce={}
    for i in arr:
        dictionary_for_ensuring_repeated_words_are_consideredonce[i]=type
        #dictionary ensures the count of every distinct word in every document as only one.
    for wordKey in dictionary_for_ensuring_repeated_words_are_consideredonce.keys():
        dataset['text'].append(wordKey)
        dataset['count'].append(1)
        dataset['type'].append(dictionary_for_ensuring_repeated_words_are_consideredonce[wordKey])

df = pd.DataFrame(dataset,columns= ['type', 'text','count'])

df_wc = df.groupby(['type', 'text']).agg({'count':'sum'}) #.to_csv("wordcount.csv",sep=",",header=True)

#df_wc=df_wc[df_wc['count']>1]

df_wc.to_csv("wordcount.csv",sep=",",header=True)

df_wordcount_without_type_for_laplace= df.groupby(['text']).agg({'count':'sum'})#.to_csv("wordcount_without_type_for_laplace.csv",sep=",",header=True)


#df_wordcount_without_type_for_laplace=df_wordcount_without_type_for_laplace[df_wordcount_without_type_for_laplace['count']>1]

df_wordcount_without_type_for_laplace.to_csv("wordcount_without_type_for_laplace.csv",sep=",",header=True)


