from nltk.corpus import stopwords
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from gensim.parsing.preprocessing import remove_stopwords

lemmatizer = WordNetLemmatizer()

class Nltk():
    words = ""
    def __init__(self,words):
        self.words=words
    def getOnlyAlpha(self,word):
        s=""
        for i in word:
            if(i.isalpha()):
                s+=i
        return s

    def removeStopwords(self):
        text_tokens = word_tokenize(self.words)

        tokens_without_sw = [lemmatizer.lemmatize(word) for word in text_tokens if not word in stopwords.words()]
        tokens_without_sw_only_alpha = [self.getOnlyAlpha(word) for word in tokens_without_sw]
        filtered_sentence = " ".join(tokens_without_sw_only_alpha)
        filtered_sentence = remove_stopwords(filtered_sentence)
        return filtered_sentence

